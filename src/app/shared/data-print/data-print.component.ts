
import { Component, Input, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-data-print',
  templateUrl: './data-print.component.html',
  styleUrls: ['./data-print.component.scss']
})
export class DataPrintComponent implements OnInit {
  @Input() title: string = '';
  @Input() text: string | undefined = '';
  @Input() value: number | Date | undefined = 0;
  @Input() type: string = '';
  

  constructor(private currencyPipe: CurrencyPipe) { }

  ngOnInit(): void {
  }

}
