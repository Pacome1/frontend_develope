import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPrintComponent } from './data-print.component';

describe('DataPrintComponent', () => {
  let component: DataPrintComponent;
  let fixture: ComponentFixture<DataPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
