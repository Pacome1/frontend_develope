import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

//Interface
import { IPlayer } from 'src/app/interfaces/football.interface';

@Injectable({
  providedIn: 'root'
})
export class ViewManagementService {
  private sidenav: MatSidenav;

  player: IPlayer = {};

  constructor() { }

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  public open(player: IPlayer) {
    this.player = player
    return this.sidenav?.open();
  }

  public close() {
    return this.sidenav.close();
  }

  public toggle(): void {
    this.sidenav.toggle();
  }
}
