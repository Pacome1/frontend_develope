import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

//Services
import { ViewManagementService } from './view-management.service';

@Component({
  selector: 'app-view-management',
  templateUrl: './view-management.component.html',
  styleUrls: ['./view-management.component.scss'],
  animations: [
    trigger('enabledStateChange', [
      state(
        'open',
        style({
          zIndex: 10200,
          opacity: 1,
          height: '100vh'
        })
      ),
      state(
        'close',
        style({
          opacity: 0,
          zIndex: 1
        })
      ),
      transition('open => *', animate('0.5s ease-in', style({ opacity: 0 })))
    ])
  ]
})
export class ViewManagementComponent implements AfterViewInit {

  @ViewChild('sideNav') sideNav!: MatSidenav;

  constructor(public viewService: ViewManagementService) {}

  ngAfterViewInit(): void {
    this.viewService.setSidenav(this.sideNav);
  }


}
