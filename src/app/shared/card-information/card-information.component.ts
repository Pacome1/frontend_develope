import { Component, Input, OnInit } from '@angular/core';

//Services
import { ViewManagementService } from '../view-management/view-management.service';

//Interface
import { IPlayer } from 'src/app/interfaces/football.interface';

@Component({
  selector: 'app-card-information',
  templateUrl: './card-information.component.html',
  styleUrls: ['./card-information.component.scss']
})
export class CardInformationComponent implements OnInit {

  @Input() player: IPlayer = {};

  constructor(public viewService: ViewManagementService) { }

  ngOnInit(): void {
  }


}
