import { Component, Input, OnInit } from '@angular/core';

//Interface
import { ITeams } from 'src/app/interfaces/football.interface';

@Component({
  selector: 'app-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss']
})
export class CollapseComponent implements OnInit {

  @Input() teams: ITeams[] = [{}];

  constructor() { }

  ngOnInit(): void {}

}
