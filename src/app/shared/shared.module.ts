import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Modules
import { MaterialModule } from '../material.module';

//Pipe
import { CurrencyPipe } from '@angular/common';

//Components
import { NavbarComponent } from './navbar/navbar.component';
import { CollapseComponent } from './collapse/collapse.component';
import { CardInformationComponent } from './card-information/card-information.component';
import { ViewManagementComponent } from './view-management/view-management.component';
import { DataPrintComponent } from './data-print/data-print.component';

const DECLARATIONS = [
  NavbarComponent,
  CollapseComponent,
  CardInformationComponent,
  ViewManagementComponent,
  DataPrintComponent
]

@NgModule({
  declarations: [ ...DECLARATIONS ],
  imports: [
    CommonModule,
    MaterialModule
    
  ],
  exports: [ ...DECLARATIONS ],
  providers: [ CurrencyPipe]
})
export class SharedModule { }
