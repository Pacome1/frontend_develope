export interface IFootball {
    teams?: INameId[],
    players?: IPlayer[],
    streaks?: {}
}

export interface ITeams {
    id?: string,
    name?: string
    players?: IPlayer[]
}

export interface INameId {
    id?: string,
    name?: string
}

export interface IPlayer {
    picture?: string,
    name?: string,
    position?: number,
    id_team?: number,
    id_comunio?: number,
    value?: number,
    points?: number,
    avg?: number,
    status?: null,
    id_competition?: number,
    ts_pic?: number,
    prev_value?: number,
    last_modified?: Date,
    id_uc?: number,
    clause?: number,
    streak?: IStreak,
    shield?: number,
    fav?: number,
    match_info?: IMatchInfo
}

export interface IStreak 
{
    3: IStreakItem,
    4: IStreakItem,
    5: IStreakItem
}

export interface IStreakItem {
    id_team?: number,
    id_player?: number,
    gameweek?: number,
    points?: number,
    color?: number
}

export interface IMatchInfo {
    is_home?: boolean,
    rival_team_id?: number
}