import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//Interface
import { IFootball, ITeams } from 'src/app/interfaces/football.interface';

@Injectable({
  providedIn: 'root'
})
export class FootballTeamsService {

  private endPoint= '';
  teams: ITeams[] = [];
  
  constructor(private http: HttpClient) { }

  getAll(): Observable<IFootball> {

    if (isDevMode()) {
      this.endPoint = 'assets/db.json';
    }
    return this.http.get<IFootball>(this.endPoint).pipe(map((data: IFootball) => data));
  }
}
