import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

//Services
import { FootballTeamsService } from 'src/app/services/football-teams/football-teams.service';

//Interface
import { IFootball, ITeams, INameId, IPlayer } from 'src/app/interfaces/football.interface';
@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];
  teams: ITeams[] = [];

  constructor(private serviceFootball: FootballTeamsService) { }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy(): void {
    if (this.subscription.length > 0) {
      this.subscription.forEach((sub: Subscription) => {sub.unsubscribe()});
    }
    
  }

  getAll(): void {
    this.subscription.push(
      this.serviceFootball.getAll().subscribe(
        (data: IFootball) => {
          data.teams?.forEach((team: INameId) => {
            
            const players = data.players?.filter((player: IPlayer) => player.id_team === team.id);

            const item: ITeams = 
            {
              id: team.id,
              name: team.name,
              players: players
            }

            this.teams.push(item);
          });
          console.log(this.teams);
        },
        (error: HttpErrorResponse) => {

        }        
      )
    );
  }
}
