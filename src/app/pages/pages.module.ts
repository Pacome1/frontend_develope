import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { MaterialModule } from '../material.module';
import { SharedModule } from './../shared/shared.module';

//components
import { SummaryComponent } from './summary/summary.component';

const DECLARATIONS = [
  SummaryComponent
]

@NgModule({
  declarations: [ ...DECLARATIONS ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule
  ],
  exports: [ ...DECLARATIONS ]
})
export class PagesModule { }
